package com.tdsilab.tdsilab.RecyclerAdapter;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tdsilab.tdsilab.Model.Atelier;
import com.tdsilab.tdsilab.R;

import java.util.ArrayList;

/**
 * Created by Moussa Diallo on 25/03/2017.
 */

public class AccueilRecycleAdapter extends RecyclerView.Adapter<AccueilRecycleAdapter.Holder> {

    private ArrayList<Atelier> ateliers;
    private Context context;

    public AccueilRecycleAdapter(Context context, ArrayList<Atelier> ateliers) {
        this.context = context;
        this.ateliers = ateliers;
    }

    @Override
    public AccueilRecycleAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(this.context).inflate(R.layout.item_accueil, parent, false));
    }

    @Override
    public void onBindViewHolder(AccueilRecycleAdapter.Holder holder, int position) {
        Atelier atelier = ateliers.get(position);

        holder.txtTitre.setText(atelier.getTitre());
        holder.txtPresentateur.setText(atelier.getPresentateur().getName());
        holder.txtHoraire.setText(atelier.getHoraire());

        holder.imgheader.setImageResource(this.context.getResources().getIdentifier("drawable/a"+atelier.getId(),null,this.context.getPackageName()));

        if(position > 0){
            holder.imgcover.setVisibility(View.GONE);
        }

        int niveau = atelier.getNiveau();
       /* if(niveau == 4) {
            holder.imgStar5.setImageResource(R.drawable.staroutline);
        }
        if(niveau == 3) {
            holder.imgStar5.setImageResource(R.drawable.staroutline);
            holder.imgStar4.setImageResource(R.drawable.staroutline);
        }
        if(niveau == 2) {
            holder.imgStar5.setImageResource(R.drawable.staroutline);
            holder.imgStar4.setImageResource(R.drawable.staroutline);
            holder.imgStar3.setImageResource(R.drawable.staroutline);
        }
        if(niveau == 1) {
            holder.imgStar5.setImageResource(R.drawable.staroutline);
            holder.imgStar4.setImageResource(R.drawable.staroutline);
            holder.imgStar3.setImageResource(R.drawable.staroutline);
            holder.imgStar2.setImageResource(R.drawable.staroutline);
        }
        if(niveau == 0) {
            holder.imgStar5.setImageResource(R.drawable.staroutline);
            holder.imgStar4.setImageResource(R.drawable.staroutline);
            holder.imgStar3.setImageResource(R.drawable.staroutline);
            holder.imgStar2.setImageResource(R.drawable.staroutline);
            holder.imgStar1.setImageResource(R.drawable.staroutline);
        }*/

    }


    @Override
    public int getItemCount() {
        return this.ateliers.size();
    }


    public  class Holder extends RecyclerView.ViewHolder {
        public TextView txtTitre;
        public TextView txtPresentateur;
        public TextView txtHoraire;
        public ImageView imgStar1;
        public ImageView imgStar2;
        public ImageView imgStar3;
        public ImageView imgStar4;
        public ImageView imgStar5;
        public ImageView imgheader;
        public ImageView imgcover;

        public Holder(View itemView) {
            super(itemView);
            this.txtTitre = (TextView)itemView.findViewById(R.id.txtTitre);
            this.txtPresentateur = (TextView)itemView.findViewById(R.id.txtPresentateur);
            this.txtHoraire = (TextView)itemView.findViewById(R.id.txtHoraire);
            /*this.imgStar1 = (ImageView)itemView.findViewById(R.id.imgStar1);
           /* this.imgStar1 = (ImageView)itemView.findViewById(R.id.imgStar1);
            this.imgStar2 = (ImageView)itemView.findViewById(R.id.imgStar2);
            this.imgStar3 = (ImageView)itemView.findViewById(R.id.imgStar3);
            this.imgStar4 = (ImageView)itemView.findViewById(R.id.imgStar4);
            this.imgStar5 = (ImageView)itemView.findViewById(R.id.imgStar5);*/
            this.imgheader = (ImageView) itemView.findViewById(R.id.imgheader);
            this.imgcover = (ImageView) itemView.findViewById(R.id.imgcover);
        }
    }
}
