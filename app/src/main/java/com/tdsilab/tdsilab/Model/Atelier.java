package com.tdsilab.tdsilab.Model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Moussa Diallo on 25/03/2017.
 */

public class Atelier implements Serializable {

    private int id;
    private String titre;
    private String description;
    private int niveau;
    private Date date;
    private Date dateCrea;
    private String horaire;
    private String lieu;
    private User presentateur;
    private int imgheader;

    public Atelier() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNiveau() {
        return niveau;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    public User getPresentateur() {
        return presentateur;
    }

    public void setPresentaeur(User presentateur) {
        this.presentateur = presentateur;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDateCrea() {
        return dateCrea;
    }

    public void setDateCrea(Date dateCrea) {
        this.dateCrea = dateCrea;
    }

    public String getHoraire() {
        return horaire;
    }

    public void setHoraire(String horaire) {
        this.horaire = horaire;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public int getImgheader() {
        return imgheader;
    }

    public void setImgheader(int imgheader) {
        this.imgheader = imgheader;
    }
}
