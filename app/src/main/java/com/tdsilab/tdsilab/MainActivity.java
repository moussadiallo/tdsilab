package com.tdsilab.tdsilab;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.tdsilab.tdsilab.Helper.RecyclerItemClickListener;
import com.tdsilab.tdsilab.Model.Atelier;
import com.tdsilab.tdsilab.Model.User;
import com.tdsilab.tdsilab.RecyclerAdapter.AccueilRecycleAdapter;
import com.tdsilab.tdsilab.Util.LinkUrl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private LinearLayoutManager accueilLayoutManager;
    ArrayList<Atelier> listAtelier;
    private AccueilRecycleAdapter accueilRecycleAdapter;
    AQuery aq;
    JSONArray ateliers = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        aq = new AQuery(this);

        recyclerView = (RecyclerView)findViewById(R.id.recyclerViewAccueil);

        // Initialisation
        listAtelier = new ArrayList<>();

        // Récuperation des données du serveur
        getData();
        // Test de modification

        //allAtelier();


        // Affichage avec l'aide de l'adaptateur
        accueilRecycleAdapter = new AccueilRecycleAdapter(this, listAtelier);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(accueilRecycleAdapter);
        accueilLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(accueilLayoutManager);

        // Listener des clics sur le recyclerView
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View paramView, int position) {
                //Toast.makeText(MainActivity.this, "position"+position, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainActivity.this, AtelierActivity.class);
                intent.putExtra("atelier", listAtelier.get(position));
                startActivity(intent);

            }
        }));
    }

    private void getData() {

        Atelier atelier = new Atelier();
        User user = new User();
        user.setName("Team Andando");
        atelier.setId(1);
        atelier.setTitre("Développement Android");
        atelier.setHoraire("09h-13h");
        atelier.setNiveau(2);
        atelier.setPresentaeur(user);
        Atelier atelier2 = new Atelier();
        User user2 = new User();
        user2.setName("Oumar Diouf");
        atelier2.setId(2);
        atelier2.setTitre("Sécurité de SI");
        atelier2.setHoraire("11h-16h");
        atelier2.setNiveau(3);
        atelier2.setPresentaeur(user2);
        Atelier atelier3 = new Atelier();
        User user3 = new User();
        atelier3.setId(3);
        user3.setName("Habib Ndiaye");
        atelier3.setTitre("Evolution of BlockChain");
        atelier3.setHoraire("08h-15h");
        atelier3.setNiveau(5);
        atelier3.setPresentaeur(user3);
        listAtelier.add(atelier);
        listAtelier.add(atelier2);
        listAtelier.add(atelier3);
    }

    private void allAtelier() {

        String url = LinkUrl.getUrl_all_atelier();

        Map<String, Object> params = new HashMap<String, Object>();
       // params.put("id_user", 1);

        aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                showResult(json);

            }
        });
    }

    private void showResult(JSONObject json) {
        Toast.makeText(getApplicationContext(), " Entrée show", Toast.LENGTH_SHORT).show();
        try {

            if (json != null) {
                 Toast.makeText(getApplicationContext(), " json not null", Toast.LENGTH_SHORT).show();
                User user;
                Atelier atelier;
                ateliers = json.getJSONArray("ateliers");

                // looping through All Products
                for (int i = 0; i < ateliers.length(); i++) {
                    JSONObject c = ateliers.getJSONObject(i);
                    //Toast.makeText(getApplicationContext(), " liste non null!", Toast.LENGTH_SHORT).show();
                    user = new User();
                    atelier = new Atelier();

                    int id = c.getInt("ID");
                    String titre = c.getString("TITRE");
                    String description = c.getString("DESCRIPTION");
                    int niveau = c.getInt("NIVEAU");
                    String horaire = c.getString("HORAIRE");
                    String lieu = c.getString("LIEU");
                    String date = c.getString("DATE");
                    String datecrea = c.getString("DATECREA");
                    int id_user = c.getInt("ID_USER");
                    String name = c.getString("NAME");
                    String email = c.getString("EMAIL");
                    String tel = c.getString("TEL");
                    String domaine = c.getString("DOMAINE");
                    String bio = c.getString("BIO");

                    user.setEmail(email);
                    user.setTel(tel);
                    user.setName(name);
                    user.setBio(bio);
                    user.setDomaine(domaine);
                    user.setIdUser(id_user);

                    atelier.setId(id);
                    atelier.setTitre(titre);
                    atelier.setDescription(description);
                    atelier.setNiveau(niveau);
                    atelier.setHoraire(horaire);
                    atelier.setLieu(lieu);
                    atelier.setPresentaeur(user);

                    listAtelier.add(atelier);
                }

                // Affichage avec l'aide de l'adaptateur
                accueilRecycleAdapter = new AccueilRecycleAdapter(this, listAtelier);
                recyclerView.setNestedScrollingEnabled(false);
                recyclerView.setAdapter(accueilRecycleAdapter);
                accueilLayoutManager = new LinearLayoutManager(this);
                recyclerView.setLayoutManager(accueilLayoutManager);
            } else {
                // no products found
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
