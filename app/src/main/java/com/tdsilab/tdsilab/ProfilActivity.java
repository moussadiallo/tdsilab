package com.tdsilab.tdsilab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tdsilab.tdsilab.Model.Atelier;
import com.tdsilab.tdsilab.Model.User;

/**
 * Created by Moussa Diallo on 26/03/2017.
 */

public class ProfilActivity extends AppCompatActivity {
    TextView txtName;
    TextView txtBio;
    TextView txtDomaine;
    TextView txtEmail;
    TextView txtTel;
    ImageView imgPresentateur;
    User presentateur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        // Bind views
        bindView();

        // Récuperer les infos qui arrivent
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            presentateur = (User) extras.get("profil");
        }

        // Setter les infos reçues
        setInfos();
    }


    private void bindView() {
        txtName = (TextView) findViewById(R.id.txtName);
        txtBio = (TextView) findViewById(R.id.txtBio);
        txtDomaine = (TextView) findViewById(R.id.txtDomaine);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtTel = (TextView) findViewById(R.id.txtTel);
        imgPresentateur = (ImageView) findViewById(R.id.imgPresentateur);
    }

    private void setInfos() {
        txtName.setText(presentateur.getName());
        txtBio.setText(presentateur.getBio());
        txtDomaine.setText(presentateur.getDomaine());
        txtEmail.setText(presentateur.getEmail());
        txtTel.setText(presentateur.getTel());
    }
}
