package com.tdsilab.tdsilab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tdsilab.tdsilab.Model.Atelier;

/**
 * Created by Moussa Diallo on 26/03/2017.
 */

public class AtelierActivity extends AppCompatActivity {
    TextView txtTitre;
    TextView txtDescription;
    TextView txtName;
    TextView txtDate;
    ImageView imgPresentateur;
    Atelier atelier;
    ImageView bgheader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atelier);

        // Bind views
        bindView();

        // Récuperer les infos qui arrivent
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            atelier = (Atelier) extras.get("atelier");
        }

        // Setter les infos reçues
        setInfos();

        // Listener pour voir le bio du présentateur
        txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AtelierActivity.this, ProfilActivity.class);
                intent.putExtra("profil", atelier.getPresentateur());
                startActivity(intent);
            }
        });
    }


    private void bindView() {
        txtTitre = (TextView) findViewById(R.id.txtTitre);
        txtDescription = (TextView) findViewById(R.id.txtDescription);
        txtName = (TextView) findViewById(R.id.txtName);
        //txtDate = (TextView) findViewById(R.id.txtDate);
        imgPresentateur = (ImageView) findViewById(R.id.imgPresentateur);
        bgheader = (ImageView) findViewById(R.id.bgheader);
    }

    private void setInfos() {
        txtTitre.setText(atelier.getTitre());
        txtDescription.setText(atelier.getDescription());
        txtName.setText(atelier.getPresentateur().getName());
        bgheader.setImageResource(this.getResources().getIdentifier("drawable/a"+atelier.getId(),null,this.getPackageName()));

    }
}
